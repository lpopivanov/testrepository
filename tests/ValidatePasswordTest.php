<?php

use PHPUnit\Framework\TestCase;

class ValidatePasswordTest extends TestCase {
    
    /**
     *  Test valid length
     */
    public function testValidLength() {
        $validPass = new ValidatePassword();
        
        $this->assertFalse($validPass->validLength('1234'));
    }
    
}